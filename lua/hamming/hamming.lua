local Hamming = {}

function Hamming.compute(a,b)
  if #a ~= #b then return -1 end
  local hamming = 0
  for index=1,#a do
      hamming = hamming + (a:sub(index, index) ~= b:sub(index, index) and 1 or 0)
  end
  return hamming
    

end

return Hamming
