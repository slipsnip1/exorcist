return {
  decode = function(c1, c2, c3)
    local colors = {black=0,
              brown=1,
              red=2,
              orange=3,
              yellow=4,
              green=5,
              blue=6,
              violet=7,
              grey=8,
              white=9}
    local value1, value2, value3 = colors[c1], colors[c2], colors[c3]
    
    local unit = 'ohms'
     
    local multiplier = math.modf(1 * 10 ^ value3)
    local value = (value1 * 1e1 + value2 * 1e0) * multiplier
    local result = 0
    local unitLookup = {[6]="megaohms", [3]="kiloohms", [0]="ohms"}
    
    for magnitude=6,0,-3 do
      local integral, fraction = math.modf(value / (1 * 10 ^ magnitude))
      if integral > 0 then
        result, unit = (integral + fraction), unitLookup[magnitude]
        break
      end
    end   
    
    return result, unit
  end
}
